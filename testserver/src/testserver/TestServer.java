package testserver;

import gps.beans.Coord;
import gps.packet.PacketMarker;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;


public class TestServer {
    
    private static Socket socket;
    private static ObjectInputStream is;
    private static ObjectOutputStream os;
    private static Random rnd = new Random();

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        InetAddress addr = InetAddress.getByName("localhost");
        socket = new Socket(addr, 12162);
        os = new ObjectOutputStream(socket.getOutputStream());
        is = new ObjectInputStream(socket.getInputStream());
        try {
           testAutorization();
        }
        finally {
           System.out.println("closing...");
           socket.close();
        }
    }
    
//    public static void testAutorization() throws IOException, ClassNotFoundException{
//        Packet recievedPacket;
//        String number = "(929) 410-0721";
////        String number = "(929) 410-0722";
//        os.writeObject(new Packet(number));
//        recievedPacket = (Packet) is.readObject();
//        System.out.println(recievedPacket.getMarker());
////        os.writeObject(new Packet("password"));
//        os.writeObject(new Packet("password1"));
//        recievedPacket = (Packet) is.readObject();
//        System.out.println(recievedPacket.getMarker());
//    }
    
    public static void testAutorization() throws IOException, ClassNotFoundException{
        //AUTORIZATION
        String number = "(929) 410-0721";
//        String number = "(929) 410-0722";
        os.writeObject(number);
        PacketMarker packetMarker = (PacketMarker) is.readObject();
        System.out.println(packetMarker);
        os.writeObject("password");
//        os.writeObject("password1");
        packetMarker = (PacketMarker) is.readObject();
        System.out.println(packetMarker);
        
        //SEND COORDINATE
        Date date = new Date(116, 9, 1);
        ArrayList<Coord> coords = new ArrayList<>();
        coords.add(new Coord(48.5027313d, 135.06625989999998d, date));
        os.writeObject(coords);
        coords = new ArrayList<Coord>();
        packetMarker = (PacketMarker) is.readObject();
        System.out.println(packetMarker);
        for(int i = 0; i < 11; i++){
            coords.add(new Coord(48.5027313d + rnd.nextFloat() * (rnd.nextBoolean() ? -1 : 1), 
                    135.06625989999998d  + rnd.nextFloat() * (rnd.nextBoolean() ? -1 : 1), new Date(116, 9, i + 2)));
        }
        os.writeObject(coords);
        packetMarker = (PacketMarker) is.readObject();
        System.out.println(packetMarker);
    }
    
}
