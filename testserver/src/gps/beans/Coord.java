
package gps.beans;

import java.io.Serializable;
import java.util.Date;


public class Coord implements Serializable {
    private double x, y;
    private Date date;

    public Coord(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Coord(double x, double y, Date date) {
        this.x = x;
        this.y = y;
        this.date = date;
    }
    
    

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return x + ", " + y;
    }
    
}
