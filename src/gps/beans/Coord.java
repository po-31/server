package gps.beans;

import java.io.Serializable;
import java.util.Date;


public class Coord implements Serializable {
    private String deviceId;
    private double x, y;
    private Date date;
    private int hash;

    public Coord(double x, double y, Date date, String deviceId) {
        this.x = x;
        this.y = y;
        this.date = date;
        this.deviceId = deviceId;
    }
    
    

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public Date getDate() {
        return date;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public int getHash() {
        return hash;
    }

    public void setHash(int hash) {
        this.hash = hash;
    }

    @Override
    public int hashCode() {
        int hachCode = 1;
        hachCode = 19 * hachCode + (this.deviceId != null ? this.deviceId.hashCode() : 0);
        hachCode = 19 * hachCode + (int) (Double.doubleToLongBits(this.x) ^ (Double.doubleToLongBits(this.x) >>> 32));
        hachCode = 19 * hachCode + (int) (Double.doubleToLongBits(this.y) ^ (Double.doubleToLongBits(this.y) >>> 32));
        hachCode = 19 * hachCode + (this.date != null ? this.date.hashCode() : 0);
        return hachCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Coord other = (Coord) obj;
        if (Double.doubleToLongBits(this.x) != Double.doubleToLongBits(other.x)) {
            return false;
        }
        if (Double.doubleToLongBits(this.y) != Double.doubleToLongBits(other.y)) {
            return false;
        }
        if ((this.deviceId == null) ? (other.deviceId != null) : !this.deviceId.equals(other.deviceId)) {
            return false;
        }
        if (this.date != other.date && (this.date == null || !this.date.equals(other.date))) {
            return false;
        }
        return true;
    }
    
    

    @Override
    public String toString() {
        return x + ", " + y;
    }
    
}