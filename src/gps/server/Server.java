package gps.server;

import gps.beans.Coord;
import gps.db.DataBase;
import gps.packet.PacketMarker;
import java.io.*;
import java.net.*;

public class Server {

    private static ServerSocket serverSocket = null;

    public static void main(String[] args) throws FileNotFoundException, IOException {
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File("connectionconfig")));
            String[] connectionConfig = br.readLine().split(" ");
            serverSocket = new ServerSocket(Integer.parseInt(connectionConfig[0]), Integer.parseInt(connectionConfig[0]),
                    InetAddress.getByName(connectionConfig[2]));
            br.close();

            br = new BufferedReader(new FileReader(new File("databaseconfig")));
            String[] databaseConfig = br.readLine().split(" ");
            DataBase.setUrl(databaseConfig[0]);
            DataBase.setName(databaseConfig[1]);
            DataBase.setPassword(databaseConfig[2]);
            br.close();

            new Thread() {
                @Override
                public void run() {
                    try {
                        System.out.println("bind success");
                        while (true) {
                            Socket socket = serverSocket.accept();
                            System.out.println("accept " + socket.toString());
                            new ServeClient(socket);
                        }
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                }
            }.start();
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    static class ServeClient extends Thread {

        private Socket socket;
        private ObjectInputStream is;
        private ObjectOutputStream os;

        public ServeClient(Socket s) throws IOException {
            socket = s;
            os = new ObjectOutputStream(socket.getOutputStream());
            is = new ObjectInputStream(socket.getInputStream());
            setDaemon(true);
            start(); // вызываем run()
        }

        public void run() {
            try {
                String number;
                number = (String) is.readObject();
                if (DataBase.isUserExist(number)) {
                    os.writeObject(PacketMarker.PASSWORD_REQUEST);
                    while (true) {
                        String password = (String) is.readObject();
                        if (!DataBase.validUser(number, password)) {
                            os.writeObject(PacketMarker.INVALID_PASSWORD);
                            continue;
                        }
                        break;
                    }
                } else {
                    os.writeObject(PacketMarker.REGISTER_REQUEST);
                    String password = (String) is.readObject();
                    DataBase.registerUser(number, password);
                }
                os.writeObject(PacketMarker.AUTORIZATION_OK);
                while (true) {
                    Coord coord = (Coord) is.readObject();
                    if (coord.getHash() != coord.hashCode() && coord.getDeviceId().equals(number)) {
                        os.writeObject(PacketMarker.ERROR);
                        return;
                    }
                    System.out.println(coord.getX() + " " + coord.getY() + " " + coord.getDate());
                    DataBase.addCoord(number, coord);
                    os.writeObject(PacketMarker.OK);
                }
            } catch (Exception ex) {
                try {
                    ex.printStackTrace();
                    os.writeObject(PacketMarker.ERROR);
                } catch (IOException ex1) {
                    ex.printStackTrace();
                }
            } finally {
                try {
                    socket.close();
                    os.close();
                    is.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
