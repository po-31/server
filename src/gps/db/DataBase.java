package gps.db;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import gps.beans.Coord;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DataBase {

    private static String url = null;
    private static String name = null;
    private static String password = null;
    private static SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        Connection conn = null;
        Class.forName("com.mysql.jdbc.Driver");
        conn = DriverManager.getConnection(url, name, password);

        return conn;
    }

    public static boolean isUserExist(String number) throws SQLException, ClassNotFoundException {
        Connection conn = getConnection();
        Statement stmt = conn.createStatement();
        boolean result = false;

        ResultSet rs = stmt.executeQuery("SELECT number FROM user WHERE number = '" + number + "'");
        if (rs.next()) {
            result = true;
        }
        rs.close();
        stmt.close();
        conn.close();
        return result;
    }

    public static boolean validUser(String number, String password) throws SQLException, ClassNotFoundException {
        Connection conn = getConnection();
        Statement stmt = conn.createStatement();
        boolean result = false;

        ResultSet rs = stmt.executeQuery(String.format("SELECT number, password FROM user WHERE number = '%s' AND password = '%s'",
                number, md5(password)));
        if (rs.next()) {
            result = true;
        }
        rs.close();
        stmt.close();
        conn.close();
        return result;
    }

    public static void registerUser(String number, String password) throws SQLException, ClassNotFoundException {
        Connection conn = getConnection();
        Statement stmt = conn.createStatement();
        stmt.executeUpdate(String.format("INSERT INTO user (number, password) VALUES ('%s', '%s')", number, md5(password)));
        stmt.executeUpdate(String.format("INSERT INTO user_group (number, `group`) VALUES ('%s', 'user')", number));
        stmt.close();
        conn.close();
    }

    public static void addCoord(String number, Coord coord) throws SQLException, ClassNotFoundException {
        Connection conn = getConnection();
        Statement stmt = conn.createStatement();
        try {
            stmt.executeUpdate(String.format("INSERT INTO coord (iduser, date, coordx, coordy) VALUES ((SELECT id FROM user WHERE "
                    + " number = '%s'), '%s', '%s', '%s')", number, dateFormater.format(coord.getDate()), coord.getX(), coord.getY()));
        } catch (MySQLIntegrityConstraintViolationException e) {
        }
        stmt.close();
        conn.close();
    }

    public static String md5(String string) {
        MessageDigest messageDigest;
        byte[] digest = new byte[0];
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(string.getBytes());
            digest = messageDigest.digest();
        } catch (NoSuchAlgorithmException e) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, e);
        }
        BigInteger bigInt = new BigInteger(1, digest);
        String md5Hex = bigInt.toString(16);
        while (md5Hex.length() < 32) {
            md5Hex = "0" + md5Hex;
        }
        return md5Hex;
    }

    public static void setUrl(String url) {
        DataBase.url = url;
    }

    public static void setName(String name) {
        DataBase.name = name;
    }

    public static void setPassword(String password) {
        DataBase.password = password;
    }

}
